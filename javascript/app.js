window.$ = window.jQuery = $;

/* vendors */
require('./vendors/lazy-load');
require('./vendors/slick-slider');

/* normal */
require('../scss/app.scss');
require('./one.js');         
require('./sections/slider');